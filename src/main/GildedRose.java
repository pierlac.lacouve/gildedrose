package main;

public class GildedRose {

	public Item[] items;

	public GildedRose(Item[] items) {
		this.items = items;
		Math.min(2, 3);
	}

	/*
	 * fonction de mise a jour des Items
	 * modifie Quality et reduit sellIn
	 */
	public void updateQuality() {
		
		int qualityDelta;
		
		for (int i = 0; i < items.length; i++) {		//Sulfuras n'est pas traite car il ne doit pas etre modifie
			
			//facteur de modification de Conjured, Aged Brie et Standard
			qualityDelta = 1;
			
			if(items[i].sellIn <= 0) {
				qualityDelta = 2;
			}
			
			//Mise a jour de quality en fonction de sellIn
			
			if (items[i].name.equals("Backstage passes to a TAFKAL80ETC concert")) {
				if (items[i].sellIn <= 0) {									//Backstage depasse
					items[i].quality = 0;
				} else if (items[i].sellIn <= 5) {							//Backstage tres avance
					items[i].quality += 3;
				} else if (items[i].sellIn <= 10) {							//Backstage avance
					items[i].quality += 2;
				} else {													//Backstage standard
					items[i].quality += 1;
				}
			} else if (items[i].name.equals("Conjured Mana Cake")) {
				items[i].quality -= qualityDelta*2;							//Conjured
			} else if (items[i].name.equals("Aged Brie")) {
				items[i].quality += qualityDelta;							//Aged Brie
			} else if (!items[i].name.equals("Sulfuras, Hand of Ragnaros")) {
				items[i].quality -= qualityDelta;							//Standard
			}

			//Mise a jour de sellIn et coupe des extremes sur quality
			if (!items[i].name.equals("Sulfuras, Hand of Ragnaros")) {
				items[i].sellIn = items[i].sellIn - 1;

				if (items[i].quality < 0) {
					items[i].quality = 0;
				} else if (items[i].quality > 50) {
					items[i].quality = 50;
				}
			}
		}
	}
}
