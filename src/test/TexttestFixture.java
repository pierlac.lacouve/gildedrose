package test;

import main.GildedRose;
import main.Item;
import java.util.Scanner;

public class TexttestFixture {

	/*
	 * Recupere le nombre de jours a afficher
	 * Pour chaque jour, affiche les Items puis les met à jour
	 */
	private static void printEvolutionPerDay(Item[] items) {

		GildedRose app = new GildedRose(items);
		
		int days = scannerInt();
        
		for (int i = 0; i < days; i++) {
            System.out.println("-------- day " + i + " --------");
            System.out.println("name, sellIn, quality");
            
            for (Item item : app.items) {
                System.out.println(item);
            }
            
            System.out.println();
            app.updateQuality();
        }
	}
	
	/*
	 * Demande a l'utilisateur le nombre de jours a afficher
	 * @return : nombre de jours a afficher
	 */
	private static int scannerInt() {
		
		Scanner myObj = new Scanner(System.in);
	    System.out.println("Starting Analysis");
	    System.out.println("Enter number of days : ");
	    
	    int days = myObj.nextInt();
		
	    myObj.close();
	    
        return days;
	}
	
	/*
	 * Initialise les Items et lance la fonction d'affichage
	 */
	public static void main(String[] args) {
        Item[] items = new Item[] {
                new Item("+5 Dexterity Vest", 10, 20),
                new Item("Aged Brie", 2, 0),
                new Item("Elixir of the Mongoose", 5, 7),
                new Item("Sulfuras, Hand of Ragnaros", 0, 80),
                new Item("Sulfuras, Hand of Ragnaros", -1, 80),
                new Item("Backstage passes to a TAFKAL80ETC concert", 15, 20),
                new Item("Backstage passes to a TAFKAL80ETC concert", 10, 49),
                new Item("Backstage passes to a TAFKAL80ETC concert", 5, 49),
                new Item("Conjured Mana Cake", 3, 6)
        };
        
        printEvolutionPerDay(items);
    }

}
