package test;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

import main.GildedRose;
import main.Item;

public class GildedRoseTest {

	/*
	 * Chaque fonction de test initialise une liste d'items et la liste attendues
	 * La liste initiale a pour but de cibler les cas extremes de chaque type d'Item (en fonction des variations de sellIn et de quality)
	 * On apelle ensuite la fonction testArryItems pour comparer les resultats attendus et obtenus
	 */
	
	@Test
	public void testStandard() {
		Item[] itemsBegin = new Item[] {
				new Item("Standard_1", 10, 2),
				new Item("Standard_2", 10, 0),
				new Item("Standard_3", 1, 10),
				new Item("Standard_4", 0, 10),
				new Item("Standard_5", -2, 0),
				new Item("Standard_6", -3, 50)
		};
		
		Item[] itemsEnd = new Item[] {
				new Item("Standard_1", 9, 1),
				new Item("Standard_2", 9, 0),
				new Item("Standard_3", 0, 9),
				new Item("Standard_4", -1, 8),
				new Item("Standard_5", -3, 0),
				new Item("Standard_6", -4, 48)
		};
		
		testArrayItems(itemsBegin, itemsEnd);
	}

	@Test
	public void testSulfuras() {
		Item[] itemsBegin = new Item[] {
				new Item("Sulfuras, Hand of Ragnaros", 1, 80)
		};
		
		Item[] itemsEnd = new Item[] {
				new Item("Sulfuras, Hand of Ragnaros", 1, 80)
		};
		
		testArrayItems(itemsBegin, itemsEnd);
	}

	@Test
	public void testBackstage() {
		Item[] itemsBegin = new Item[] {
				new Item("Backstage passes to a TAFKAL80ETC concert", 20, 0),
				new Item("Backstage passes to a TAFKAL80ETC concert", 15, 50),
				new Item("Backstage passes to a TAFKAL80ETC concert", 11, 0),
				new Item("Backstage passes to a TAFKAL80ETC concert", 10, 0),
				new Item("Backstage passes to a TAFKAL80ETC concert", 8, 50),
				new Item("Backstage passes to a TAFKAL80ETC concert", 6, 0),
				new Item("Backstage passes to a TAFKAL80ETC concert", 5, 0),
				new Item("Backstage passes to a TAFKAL80ETC concert", 3, 48),
				new Item("Backstage passes to a TAFKAL80ETC concert", 1, 0),
				new Item("Backstage passes to a TAFKAL80ETC concert", 0, 50)
		};
		
		Item[] itemsEnd = new Item[] {
				new Item("Backstage passes to a TAFKAL80ETC concert", 19, 1),
				new Item("Backstage passes to a TAFKAL80ETC concert", 14, 50),
				new Item("Backstage passes to a TAFKAL80ETC concert", 10, 1),
				new Item("Backstage passes to a TAFKAL80ETC concert", 9, 2),
				new Item("Backstage passes to a TAFKAL80ETC concert", 7, 50),
				new Item("Backstage passes to a TAFKAL80ETC concert", 5, 2),
				new Item("Backstage passes to a TAFKAL80ETC concert", 4, 3),
				new Item("Backstage passes to a TAFKAL80ETC concert", 2, 50),
				new Item("Backstage passes to a TAFKAL80ETC concert", 0, 3),
				new Item("Backstage passes to a TAFKAL80ETC concert", -1, 0)
		};
		
		testArrayItems(itemsBegin, itemsEnd);
	}

	@Test
	public void testAgedBrie() {
		Item[] itemsBegin = new Item[] {
				new Item("Aged Brie", 10, 50),
				new Item("Aged Brie", 10, 0),
				new Item("Aged Brie", 0, 0),
				new Item("Aged Brie", -1, 50),
				new Item("Aged Brie", -2, 0)
		};
		
		Item[] itemsEnd = new Item[] {
				new Item("Aged Brie", 9, 50),
				new Item("Aged Brie", 9, 1),
				new Item("Aged Brie", -1, 2),
				new Item("Aged Brie", -2, 50),
				new Item("Aged Brie", -3, 2)
		};
		
		testArrayItems(itemsBegin, itemsEnd);
	}

	@Test
	public void testConjured() {
		Item[] itemsBegin = new Item[] {
				new Item("Conjured Mana Cake", 10, 50),
				new Item("Conjured Mana Cake", 10, 3),
				new Item("Conjured Mana Cake", 10, 1),
				new Item("Conjured Mana Cake", 10, 0),
				new Item("Conjured Mana Cake", 1, 10),
				new Item("Conjured Mana Cake", 1, 1),
				new Item("Conjured Mana Cake", 0, 10),
				new Item("Conjured Mana Cake", 0, 3),
				new Item("Conjured Mana Cake", -1, 10)
		};
		
		Item[] itemsEnd = new Item[] {
				new Item("Conjured Mana Cake", 9, 48),
				new Item("Conjured Mana Cake", 9, 1),
				new Item("Conjured Mana Cake", 9, 0),
				new Item("Conjured Mana Cake", 9, 0),
				new Item("Conjured Mana Cake", 0, 8),
				new Item("Conjured Mana Cake", 0, 0),
				new Item("Conjured Mana Cake", -1, 6),
				new Item("Conjured Mana Cake", -1, 0),
				new Item("Conjured Mana Cake", -2, 6)
		};
		
		testArrayItems(itemsBegin, itemsEnd);
	}

	/*
	 * Appelle la fonction updateQuality sur la liste d'Items initiale
	 * Puis compare avec assertEquals la liste obtenue et la liste attendue item par item
	 */
	public void testArrayItems(Item[] initialItems, Item[] expectedItems) {
		
		//Vérification de la  non effectuée par un assertEquals car dépendant des items entrés par l'utilisateur de la fonction
		if(initialItems.length==expectedItems.length) {
			GildedRose resultItems = new GildedRose(initialItems);
			
			resultItems.updateQuality();
			
			for(int i=0;i<resultItems.items.length;++i) {
				assertEquals(expectedItems[i].toString(),resultItems.items[i].toString());
			}
		}
	}
}
